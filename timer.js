/* Script for setting a timer */


let timer = {

    timeLeft: function($endDate, $domObject){

        let update = setInterval(function(){

            // Get the difference between the targetDate and now
            let now = new Date().getTime();
            let diff = new Date($endDate - now);
            
            // If difference is above 0 (The targetDate has passed)
            if(diff > 0){
                 // Days: (difference / (miliseconds -> minutes -> hours -> days))
                // Math.get[timeInterval] for the rest.
                var days = Math.floor(diff / (1000 * 60 * 60 * 24));
                var hours = diff.getHours();
                var minutes = diff.getMinutes();
                var seconds = diff.getSeconds();

                let returnVal =  days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
                
                document.getElementById($domObject).innerHTML = returnVal;
            }
            
            

        }, 1000);
    }
}